class Cheat < ActiveRecord::Migration
	def self.up
		Station.create(:line => 'Red', :platform_key => 'RALES', :platform_name => 'ALEWIFE NB', :station_name => 'ALEWIFE', :platform_order => 0, :start_of_line => 'TRUE', :end_of_line => 'FALSE', :branch => 'Trunk', :direction => 'SB', :stop_id => 'place-alfcl', :stop_code => '', :stop_name => 'Alewife Station', :stop_desc => '', :stop_lat => 42.395428, :stop_lon => -71.142483)
		Station.create(:line => 'Red', :platform_key => 'RASHN', :platform_name => 'ASHMONT SB', :station_name => 'ASHMONT', :platform_order => 0, :start_of_line => 'TRUE', :end_of_line => 'FALSE', :branch => 'Ashmont', :direction => 'NB', :stop_id => 'place-asmnl', :stop_code => '', :stop_name => 'Ashmont Station', :stop_desc => '', :stop_lat => 42.284652, :stop_lon => -71.064489)
		Station.create(:line => 'Red', :platform_key => 'RBRAN', :platform_name => 'BRAINTREE SB', :station_name => 'BRAINTREE', :platform_order => 0, :start_of_line => 'TRUE', :end_of_line => 'FALSE', :branch => 'Braintree', :direction => 'NB', :stop_id => 'place-brntn', :stop_code => '', :stop_name => 'Braintree Station', :stop_desc => '', :stop_lat => 42.2078543, :stop_lon => -71.0011385)
		Station.create(:line => 'Orange', :platform_key => 'OOAKS', :platform_name => 'OAK GROVE NB', :station_name => 'OAK GROVE', :platform_order => 0, :start_of_line => 'TRUE', :end_of_line => 'FALSE', :branch => 'Trunk', :direction => 'SB', :stop_id => 'place-ogmnl', :stop_code => '', :stop_name => 'Oak Grove Station', :stop_desc => '', :stop_lat => 42.43668, :stop_lon => -71.071097)
		Station.create(:line => 'Orange', :platform_key => 'OFORN', :platform_name => 'FOREST HILLS SB', :station_name => 'FOREST HILLS', :platform_order => 0, :start_of_line => 'TRUE', :end_of_line => 'FALSE', :branch => 'Trunk', :direction => 'NB', :stop_id => 'place-forhl', :stop_code => '', :stop_name => 'Forest Hills Station', :stop_desc => '', :stop_lat => 42.300523, :stop_lon => -71.113686)
		Station.create(:line => 'Blue', :platform_key => 'BWONW', :platform_name => 'WONDERLAND EB', :station_name => 'WONDERLAND', :platform_order => 0, :start_of_line => 'TRUE', :end_of_line => 'FALSE', :branch => 'Trunk', :direction => 'WB', :stop_id => 'place-wondl', :stop_code => '', :stop_name => 'Wonderland Station', :stop_desc => '', :stop_lat => 42.41342, :stop_lon => -70.991648)
		#s = Station.find_by_platform_key('RDAVS')
		#s.start_of_line = 'FALSE'
		#s.save
		s = Station.find_by_platform_key('RSHAN')
		s.start_of_line = 'FALSE'
		s.save
		s = Station.find_by_platform_key('RQUAN')
		s.start_of_line = 'FALSE'
		s.save
		s = Station.find_by_platform_key('OMALS')
		s.start_of_line = 'FALSE'
		s.save
		s = Station.find_by_platform_key('OGREN')
		s.start_of_line = 'FALSE'
		s.save
		s = Station.find_by_platform_key('BREVW')
		s.start_of_line = 'FALSE'
		s.save
	end

	def self.down
		
	end
end

class UpdateStationDirections < ActiveRecord::Migration
	def up
		Station.update_all("direction = 'NORTHBOUND'", "direction = 'NB'")
		Station.update_all("direction = 'EASTBOUND'", "direction = 'EB'")
		Station.update_all("direction = 'SOUTHBOUND'", "direction = 'SB'")
		Station.update_all("direction = 'WESTBOUND'", "direction = 'WB'")
	end

	def down
	end
end

class OverhaulDatabase < ActiveRecord::Migration
	# Fields to keep: line, stop_name, stop_desc, stop_lat, stop_lon 
	def up
		 remove_column :stations, :platform_key
		 remove_column :stations, :platform_name
		 remove_column :stations, :station_name
		 remove_column :stations, :platform_order
		 remove_column :stations, :start_of_line
		 remove_column :stations, :end_of_line
		 remove_column :stations, :branch
		 remove_column :stations, :direction
		 remove_column :stations, :stop_id
		 remove_column :stations, :stop_code
		 Station.delete_all
	end

	def down
	end
end

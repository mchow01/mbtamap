require 'csv'
# See http://stackoverflow.com/questions/9367697/csv-on-heroku-cedar

csv_file_path = 'db/stations.csv'

Station.delete_all
CSV.foreach(csv_file_path) do |row|
	Station.create!({
		:line => row[0],
        :stop_name => row[1],
        :stop_desc => row[2],
        :stop_lat => row[3],
        :stop_lon =>row[4],
        :created_at => DateTime.now,
        :updated_at => DateTime.now})
end

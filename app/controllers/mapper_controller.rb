require 'json'
require 'net/http'
require 'time_diff' # Manual at https://github.com/abhidsm/time_diff

class MapperController < ApplicationController

	caches_page :index
	
	def index
		if Rails.cache.exist?("redline")
			@red = Rails.cache.read("redline")
		else
			@red = Station.find_all_by_line("Red")
			Rails.cache.write("redline", @red)
		end
		
		if Rails.cache.exist?("orangeline")
			@orange = Rails.cache.read("orangeline")
		else
			@orange = Station.find_all_by_line("Orange")
			Rails.cache.write("orangeline", @orange)
		end

		if Rails.cache.exist?("blueline")
			@blue = Rails.cache.read("blueline")
		else
			@blue = Station.find_all_by_line("Blue")
			Rails.cache.write("blueline", @blue)
		end
		
		if Rails.cache.exist?("commrail")
			@commrail = Rails.cache.read("commrail")
		else
			@commrail = Station.find_all_by_line("CR").uniq
			Rails.cache.write("commrail", @commrail)
		end
	end
	
	def find_closest_stations
		stations = []
		lat = params[:lat]
		lon = params[:lon]
		origin = [lat, lon]
		if !lat.nil? and !lon.nil?
			# The good news: no more limiting to within 5 miles from origin
			# The bad news: with geokit-rails, distance value is gone, need to calculate TWICE --WTF!
			stations = Station.by_distance(:origin => origin).limit(10)
			stations.each do |s|
				s['distance'] = s.distance_to(origin)
			end
		end
		respond_to do |format|
			format.json {render :layout => false,
			:json => stations.to_json()}
			format.xml {render :layout => false,
			:xml => stations.to_xml()}
		end
	end

	def station_schedule
		# See http://www.eot.state.ma.us/developers/ for APIs and doc
		commuter_rail_apis = {'Middleborough/Lakeville' => 'http://developer.mbta.com/lib/RTCR/RailLine_3.json',
		'Haverhill' => 'http://developer.mbta.com/lib/RTCR/RailLine_11.json',
		'Kingston/Plymouth' => 'http://developer.mbta.com/lib/RTCR/RailLine_2.json',
		'Lowell' => 'http://developer.mbta.com/lib/RTCR/RailLine_10.json',
		'Greenbush' => 'http://developer.mbta.com/lib/RTCR/RailLine_1.json',
		'Needham' => 'http://developer.mbta.com/lib/RTCR/RailLine_7.json',
		'Fairmount' => 'http://developer.mbta.com/lib/RTCR/RailLine_4.json',
		'Fitchburg/South Acton' => 'http://developer.mbta.com/lib/RTCR/RailLine_9.json',
		'Framingham/Worcester' => 'http://developer.mbta.com/lib/RTCR/RailLine_8.json',
		'Newburyport/Rockport' => 'http://developer.mbta.com/lib/RTCR/RailLine_12.json',
		'Providence/Stoughton' => 'http://developer.mbta.com/lib/RTCR/RailLine_5.json',
		'Franklin' => 'http://developer.mbta.com/lib/RTCR/RailLine_6.json'}
		
		# Input parameter: stop_name
		# Deprecated parameter: stop_id
		# See http://stackoverflow.com/questions/5629402/how-to-test-if-parameters-exist-in-rails
		@schedule = [];
		if (params.has_key?(:stop_name))
			station = Station.find_all_by_stop_name(params[:stop_name])
		else
			[]
		end

		@schedule = []
		station.each do |s|
			# Commuter Rail: tricky as a station can be on multiple lines!
			if (s['line'] == 'CR')
				# Load data from API
				# RTF API schema at http://www.mbta.com/uploadedfiles/devguide.pdf
				begin
					resp = Net::HTTP.get_response(URI.parse(commuter_rail_apis[s['stop_desc']]))
					data = resp.body
					commrail = JSON.parse(data)
					commrail = commrail['Messages'].select {|e| e['Stop'] == s['stop_name']}
				rescue
					commrail = []
				end
				commrail.each do |entry|
					flag = ''
					if entry['Lateness'].to_i > 0
						flag = ' (LATE)'
					end
					@schedule << {'line' => s['line'], 'direction' => entry['Destination'], 'information_type' => entry['Flag'], 'vehicle' => entry['Vehicle'], 'vehicle-last-latitude' => entry['Latitude'], 'vehicle-last-longitude' => entry['Longitude'], 'time_remaining' => Time.diff(Time.now, Time.at(entry['Scheduled'].to_i + entry['Lateness'].to_i))[:diff], 'trip' => entry['Trip'] + flag}
				end
			# Subway Trains (Red, Orange, Blue Lines)
			else
				list = []
				# "Query" the correct hash with the latest train schedule
				begin
					case s.line # Either Red, Orange, or Blue
					when 'Red'
						resp = Net::HTTP.get_response(URI.parse("http://developer.mbta.com/lib/rthr/red.json"))
					when 'Orange'
						resp = Net::HTTP.get_response(URI.parse("http://developer.mbta.com/lib/rthr/orange.json"))
					when 'Blue'
						resp = Net::HTTP.get_response(URI.parse("http://developer.mbta.com/lib/rthr/blue.json"))
					else
						resp = "[]"
					end
					data = resp.body
					theJSON = JSON.parse(data)
				rescue
					theJSON = []
				end
				begin
					theJSON['TripList']['Trips'].each do |trip|
						predictions = trip['Predictions'].select{|p| p['Stop'] == s.stop_name}
						predictions.each do |p|
							# See http://stackoverflow.com/questions/4175733/convert-duration-to-hoursminutesseconds-or-similar-in-rails-3-or-ruby
							@schedule << {'line' => s.line, 'direction' => trip['Destination'], 'time_remaining' => Time.at(p['Seconds']).utc.strftime("%H:%M:%S"), 'trip' => trip['TripID']}
						end
					end
				rescue
					@schedule = []
				end
			end
		end
		@schedule.sort!{|a, b| a['time_remaining'] <=> b['time_remaining']}
		
		respond_to do |format|
			format.json {render :layout=>false,
						:json => @schedule.to_json()}
			format.xml {render :layout=>false,
						:xml => @schedule.to_xml()}
		end
	end
end

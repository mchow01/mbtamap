#/usr/bin/python

infile = open("commuter_rail.tsv", "r")
lines = infile.readlines()
for l in lines:
	#route_long_name	direction_id	stop_sequence	stop_id	stop_lat	stop_lon	Branch
	# Example: Newburyport/Rockport	1	2	Gloucester	42.616	-70.668767	Trunk
	[route, direction_id, stop_sequence, stop_id, stop_lat, stop_lon, branch] = l.split('\t')
	branch = branch.rstrip()

	# Output example: Station.create(:line => 'Red', :platform_key => 'RALEN', :platform_name => 'ALEWIFE NB', :station_name => 'ALEWIFE', :platform_order => 17, :start_of_line => 'FALSE', :end_of_line => 'TRUE', :branch => 'Trunk', :direction => 'NB', :stop_id => 'place-alfcl', :stop_code => '', :stop_name => 'Alewife Station', :stop_desc => '', :stop_lat => 42.395428, :stop_lon => -71.142483)
	if direction_id == '0':
		direction = 'OUTBOUND'
	else:
		direction = 'INBOUND'
	platform_name = stop_id.upper() + " " + direction
	print "\t\tStation.create(:line => 'CR', :platform_key => '^" + platform_name + "', :platform_name => '" + platform_name + "', :station_name => '" + stop_id.upper() + "', :platform_order => " + stop_sequence + ", :branch => '" + branch + "', :direction => '" + direction + "', :stop_id => '" + stop_id + "', :stop_code => '', :stop_name => '" + stop_id + "', :stop_desc => '" + route + "', :stop_lat => " + stop_lat + ", :stop_lon => " + stop_lon + ")"
	
infile.close()

Provides closest station and station schedule APIs for web, Android, and iOS apps.

Uses the official MBTA APIs for Red, Orange, and Blue lines and the official MBTA APIs for Commuter Rail.

* MBTA Real-Time Commuter Rail API information: http://www.mbta.com/rider_tools/developers/default.asp?id=21899
* MBTA Real-Time Subway API version 2 information: http://www.mbta.com/uploadedfiles/Description.pdf

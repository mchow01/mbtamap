MbtamapR3::Application.routes.draw do
  get "mapper/index"

  get "mapper/find_closest_stations"

  get "mapper/station_schedule"

  root :to => "mapper#index"
end
